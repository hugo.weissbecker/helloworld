FROM busybox
RUN mkdir -p /usr/local/sbin
COPY helloworld /usr/local/sbin
ENV arg="World"
ENTRYPOINT helloworld $arg
